## Command Examples

```
CLI:
  mauth add user
  mauth delete user
  mauth key user
  mauth revoke key
  mauth password user
  mauth show [user]
  mauth server

SERVER:
  GET / - Performs "Basic" authentication:
    + For values with a ":", assume username/password and issue an auth cookie+token.
    + All other values assume API key and validate against cache or database.
  DELETE / - Immediately expires a given session token

DATASOURCE:
  create()
  delete()
  get()
  list()
  authenticate(string)  // user/pass
  validate(string)      // key
  password(string)      // set password
  key(string)           // generate API key for a user
  revoke(string)        // revoke key

INTERNAL:
  Normal:
    exists()
    list()
    get()
  Transaction
    exists()
    get()
    create()
    delete ()
```