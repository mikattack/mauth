package commands

import (
  "fmt"
  "github.com/spf13/cobra"
  "gitlab.com/mikattack/mauth/datasource"
)


var addCmd = &cobra.Command {
  Use:    "add <username>",
  Short:  "Add a new user",
  Long:   "Add a new user",
  RunE:   AddUser,
}


func AddUser(cmd *cobra.Command, args []string) error {
  db, connectErr := connectToDatabase(dbpath)
  if connectErr != nil {
    return fmt.Errorf("database connect error: %s", connectErr.Error())
  }

  if len(args) < 1 {
    return NewUserError("user name needs to be provided")
  }
  ds := datasource.GetDatasource(db)
  if err := ds.Create(args[0]); err != nil {
    return err
  }
  return nil
}
