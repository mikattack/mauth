package commands

import (
  "fmt"
  "github.com/spf13/cobra"
  "gitlab.com/mikattack/mauth/datasource"
)


var delCmd = &cobra.Command {
  Use:    "delete <username>",
  RunE:   DeleteUser,
  Short:  "Remove a user",
  Long:   "Remove a user",
}


func DeleteUser(cmd *cobra.Command, args []string) error {
  db, connectErr := connectToDatabase(dbpath)
  if connectErr != nil {
    return fmt.Errorf("database connect error: %s", connectErr.Error())
  }
  
  if len(args) < 1 {
    return NewUserError("user name needs to be provided")
  }
  ds := datasource.GetDatasource(db)
  if err := ds.Delete(args[0]); err != nil {
    return err
  }
  return nil
}
