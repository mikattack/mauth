package commands

import "regexp"


// Regex for catching some of the obvious user errors from Cobra.
// We don't want to show the usage message for every error.
var userErrorRegexp = regexp.MustCompile("argument|flag|shorthand")


type CommandlineError struct {
	message		string
	userError	bool
}

func (e CommandlineError) Error() string {
	return e.message
}

func (e CommandlineError) UserError() bool {
	return userErrorRegexp.MatchString(e.message)
}

func NewUserError(message string) CommandlineError {
	return CommandlineError { message, true }
}
