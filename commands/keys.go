package commands

import (
  "fmt"

  "github.com/spf13/cobra"
  "gitlab.com/mikattack/mauth/datasource"
)


var keyCmd = &cobra.Command {
  Use:    "key <username>",
  Short:  "Generate an API key for a user",
  Long:   "Generate an API key for a user",
  RunE:   GenerateKey,
}

var revokeKeyCmd = &cobra.Command {
  Use:    "revoke <key>",
  Short:  "Revokes an API key",
  Long:   "Revokes an API key",
  RunE:   RevokeKey,
}


func GenerateKey(cmd *cobra.Command, args []string) error {
  db, connectErr := connectToDatabase(dbpath)
  if connectErr != nil {
    return fmt.Errorf("database connect error: %s", connectErr.Error())
  }

  if len(args) < 1 {
    return NewUserError("username needs to be provided")
  }
  ds := datasource.GetDatasource(db)
  username := args[0]

  if ds.Exists(username) == false {
    return fmt.Errorf("user '%s' not found", username)
  }

  key, err := ds.GenerateKey(username)
  if err != nil {
    return err
  }

  fmt.Printf("Added key: %s", key.Key)
  return nil
}


func RevokeKey(cmd *cobra.Command, args []string) error {
  db, connectErr := connectToDatabase(dbpath)
  if connectErr != nil {
    return fmt.Errorf("database connect error: %s", connectErr.Error())
  }
  
  if len(args) < 1 {
    return NewUserError("key needs to be provided")
  }
  ds := datasource.GetDatasource(db)
  key := args[0]

  return ds.RevokeKey(key)
}
