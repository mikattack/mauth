package commands

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/howeyc/gopass"
	"gitlab.com/mikattack/mauth/datasource"
)


var passwdCmd = &cobra.Command {
	Use:    "passwd [user]",
	RunE:   Passwd,
	Short:  "Change a user's password",
	Long:   "Change a user's password",
}


func Passwd(cmd *cobra.Command, args []string) error {
	db, connectErr := connectToDatabase(dbpath)
	if connectErr != nil {
		return fmt.Errorf("database connect error: %s", connectErr.Error())
	}
  
	if len(args) < 1 {
		return NewUserError("user name needs to be provided")
	}

	name := args[0]
	ds := datasource.GetDatasource(db)

	var (
		input []byte
		confirm []byte
		err error
	)

	// Make sure user exists
	if ds.Exists(name) == false {
		return fmt.Errorf("user '%s' not found", name)
	}

	// Fetch password from command line
	fmt.Println("Enter new password:")
	input, err = gopass.GetPasswd()
	fmt.Println("Confirm new password:")
	confirm, err = gopass.GetPasswd()

	if err != nil {
		return err
	}
	if string(input) != string(confirm) {
		return fmt.Errorf("passwords do not match")
	}

	// Set password
	if err = ds.Password(name, string(input)); err != nil {
		return err
	}

	return nil
}
