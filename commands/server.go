package commands

import (
	"fmt"
	"os"
	"time"
	
	"github.com/spf13/cobra"

	"gitlab.com/mikattack/mauth/datasource"
	"gitlab.com/mikattack/mauth/server"
	"gitlab.com/mikattack/mlog"
)


const DEFAULT_REALM string = "restricted"


var (
	debug bool
	flush int
	maxSessions int
	port int
	realm string
	ttl int64
	verbose bool
)


var serverCmd = &cobra.Command {
	Use:    "server",
	RunE:   StartServer,
	Short:  "Start an HTTP authentication server",
	Long:   `Start an HTTP authentication server that tracks client sessions

Access data is emitted to STDOUT while debug, info, warnings, and errors are
sent to STDERR.
`,
}


func init() {
	serverCmd.Flags().BoolVar(&debug, "debug", false, "Adds debugging output to logging")
	serverCmd.Flags().IntVar(&flush, "flush", 3600, "Time in seconds between session cache evictions")
	serverCmd.Flags().IntVar(&port, "port", 48282, "Port to run HTTP server on")
	serverCmd.Flags().StringVar(&realm, "realm", DEFAULT_REALM, "HTTP Basic authentication realm name")
	serverCmd.Flags().IntVar(&maxSessions, "sessions", server.DEFAULT_MAX_SESSIONS, "Maximum number of active sessions (including API keys)")
	serverCmd.Flags().Int64Var(&ttl, "ttl", server.DEFAULT_SESSION_TTL, "Maximum session age")
	serverCmd.Flags().BoolVar(&verbose, "verbose", false, "Enable verbose logging")
}


func StartServer(cmd *cobra.Command, args []string) error {
	// Configure mlog messages for STDERR
	mlog.SetOutput(mlog.LEVEL_DEBUG, os.Stderr)
	mlog.SetOutput(mlog.LEVEL_INFO, os.Stderr)
	mlog.SetOutput(mlog.LEVEL_WARN, os.Stderr)
	mlog.SetOutput(mlog.LEVEL_ERROR, os.Stderr)
	if verbose == true {
		mlog.SetThreshold(mlog.LEVEL_INFO)
	}
	if debug == true {
		mlog.SetThreshold(mlog.LEVEL_DEBUG)
	}

	db, connectErr := connectToDatabase(dbpath)
	if connectErr != nil {
		return fmt.Errorf("database connect error: %s", connectErr.Error())
	}

	opts := server.Options {
		FlushPeriod:		time.Duration(flush) * time.Second,
		MaxSessions:		maxSessions,
		Port:						port,
		Realm:					realm,
		TTL:						ttl,
	}
	mlog.INFO.Printf("Using database: %s", dbpath)
	s := server.New(datasource.GetDatasource(db), opts)
	s.Start()
	return nil
}
