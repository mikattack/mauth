package commands

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/mikattack/mauth/datasource"
)


var showCmd = &cobra.Command {
	Use:		"show [user]",
	Short:	"List registered users",
	Long:		"Lists all users or a single user",
	RunE:		Show,
}


func Show(cmd *cobra.Command, args []string) error {
	if len(args) == 1 {
		return ShowUser(args[0])
	} else {
		return ListUsers()
	}
}


func ShowUser(username string) error {
	db, connectErr := connectToDatabase(dbpath)
	if connectErr != nil {
		return fmt.Errorf("database connect error: %s", connectErr.Error())
	}

	ds := datasource.GetDatasource(db)
	user, err := ds.Get(username);
	if err != nil {
		if err == datasource.ErrNotFound {
			return fmt.Errorf("user '%s' not found", username)
		}
		return err
	}
	
	fmt.Printf("Name: %s\n", user.Name)

	fmt.Println("Keys:")
	if keys, err := ds.ListKeys(username); err != nil {
		return err
	} else if len(keys) == 0 {
		fmt.Println("  No keys assigned")
	} else {
		for _, key := range keys {
			fmt.Printf("  %s\n", key.Key)
		}
	}
	return nil
}


func ListUsers() error {
	db, connectErr := connectToDatabase(dbpath)
	if connectErr != nil {
		return fmt.Errorf("database connect error: %s", connectErr.Error())
	}
  
	ds := datasource.GetDatasource(db)
	if list, err := ds.List(); err != nil {
		return err
	} else {
		for _, item := range list {
			fmt.Printf("%s\n", item)
		}
	}
	return nil
}
