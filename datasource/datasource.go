package datasource

import (
	"bytes"
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/scrypt"
)

const DEFAULT_TIMEOUT int = 30
const DEFAULT_SCHEMA string = `
	PRAGMA foreign_keys = ON;
	CREATE TABLE IF NOT EXISTS mauth_users (
		name		TEXT primary key,
		passwd	TEXT DEFAULT "",
		salt		TEXT DEFAULT "",
		created DATETIME DEFAULT CURRENT_TIMESTAMP
	);
	CREATE TABLE IF NOT EXISTS mauth_keys (
		user		TEXT,
		key			TEXT,
		created	DATETIME DEFAULT CURRENT_TIMESTAMP,
		FOREIGN KEY (user) REFERENCES mauth_users(name) ON DELETE CASCADE,
		PRIMARY KEY (user, key)
	);
`


type transactionFunc func(*sqlx.Tx) error

var ErrNotFound = errors.New("not found")

type Datasource struct{
	conn *sqlx.DB
}


func GetDatasource(connection *sqlx.DB) *Datasource {
	return &Datasource{ conn:connection }
}


// Authenticates a username and password.
func (ds *Datasource) Authenticate(user string, passwd string) (bool, error) {
	var (
		hash string
		err error
		result struct {
			Passwd	string	`db:"passwd"`
			Salt		string	`db:"salt"`
		}
	)
	query := "SELECT passwd, salt FROM mauth_users WHERE name = ?"
	err = ds.conn.Get(&result, query, user)
	if err == sql.ErrNoRows {
		return false, fmt.Errorf("user '%s' not found", user)
	}
	if err != nil {
		return false, err
	}
	if hash, _, err = generatePassword(user, passwd, result.Salt); err != nil {
		return false, err
	}
	if len(hash) == 0 {
		// Empty passwords should not authenticate
		return false, fmt.Errorf("cannot authenticate against empty password")
	}
	return (hash == result.Passwd), nil
}


// Inserts new users (updates are unsupported in this application)
func (ds *Datasource) Create(username string) error {
	return transaction(ds.conn, func (tx *sqlx.Tx) error {
		sql := `INSERT INTO mauth_users (name) VALUES (?)`
		if stmt, err := tx.Prepare(sql); err != nil {
			return err
		} else if _, err := stmt.Exec(username); err != nil {
			sqliteErr := err.(sqlite3.Error)
			if sqliteErr.Code == sqlite3.ErrConstraint {
				// Duplicate record is not a failure
				return nil
			}
			return err
		}
		return nil
	})
}


// Checks whether a user exists
func (ds *Datasource) Exists(username string) bool {
	var count int
	sql := `SELECT count(name) AS results FROM mauth_users WHERE name=?`
	err := ds.conn.Get(&count, sql, username)
	return err == nil && count > 0
}


// Checks whether a user exists within a transaction
func txExists(conn *sqlx.Tx, username string) bool {
	var count int
	sql := `SELECT count(name) AS results FROM mauth_users WHERE name=?`
	err := conn.Get(&count, sql, username)
	return err == nil && count > 0
}


// Fetches the values of a single user from a given table.
func (ds *Datasource) Get(username string) (User, error) {
	var user User
	query := `SELECT * FROM mauth_users WHERE name=?`
	err := ds.conn.QueryRowx(query, username).StructScan(&user)
	if err != nil && err == sql.ErrNoRows {
		return user, ErrNotFound
	} else {
		return user, err
	}
}


// Removes a single user
func (ds *Datasource) Delete(username string) error {
	return transaction(ds.conn, func (tx *sqlx.Tx) error {
		sql := `DELETE FROM mauth_users WHERE name = ?`
		if stmt, err := tx.Prepare(sql); err != nil {
			return err
		} else if _, err := stmt.Exec(username); err != nil {
			return err
		}
		return nil
	})
}


// Lists all users.
func (ds *Datasource) List() ([]string, error) {
	var results []string
	sql := `SELECT name FROM mauth_users ORDER BY name`
	err := ds.conn.Select(&results, sql)
	if err != nil {
		return nil, err
	}
	return results, nil
}


// Changes the password of a user
func (ds *Datasource) Password(username string, passwd string) error {
	var (
		hash string
		salt string
		err error
	)
	if hash, salt, err = generatePassword(username, passwd); err != nil {
		return err
	}
	return transaction(ds.conn, func (tx *sqlx.Tx) error {
		if txExists(tx, username) == false {
			return ErrNotFound
		}
		sql := `UPDATE mauth_users SET passwd = ?, salt = ? WHERE name = ?`
		if stmt, err := tx.Prepare(sql); err != nil {
			return err
		} else if _, err = stmt.Exec(hash, salt, username); err != nil {
			return err
		}
		return nil
	})
}


// Creates a new API key for a user
func (ds *Datasource) GenerateKey(username string) (ApiKey, error) {
  k := ApiKey{}
  err := transaction(ds.conn, func (tx *sqlx.Tx) error {
    var kerr error
    k, kerr = NewApiKey(username)
    if kerr != nil {
      return kerr
    }

    if txExists(tx, username) == false {
    	return ErrNotFound
    }

    sql := `INSERT INTO mauth_keys (user, key) VALUES (?,?)`
    if stmt, err := tx.Prepare(sql); err != nil {
      return err
    } else if _, err := stmt.Exec(username, k.Key); err != nil {
      return err
    }
    return nil
  })
  return k, err
}


// Revokes an API key
func (ds *Datasource) RevokeKey(keys ...string) error {
	if len(keys) == 0 {
		return nil
	}
  return transaction(ds.conn, func (tx *sqlx.Tx) error {
  	sql := `DELETE FROM mauth_keys WHERE key = ?`
    stmt, err := tx.Prepare(sql)
    if err != nil {
      return err
    }
  	for _, key := range keys {
	     if _, err := stmt.Exec(key); err != nil {
	      return err
	    }
	  }
    return nil
  })
}


func (ds *Datasource) ListKeys(username string) ([]ApiKey, error) {
	var results []ApiKey
	sql := `SELECT * FROM mauth_keys WHERE user = ? ORDER BY key`
	err := ds.conn.Select(&results, sql, username)
	if err != nil {
		return []ApiKey{}, err
	}
	return results, nil
}


// Confirm an API key is valid
func (ds *Datasource) ValidateKey(key string) bool {
  var count int
  sql := `SELECT count(key) AS results FROM mauth_keys WHERE key=?`
  err := ds.conn.Get(&count, sql, key)
  return err == nil && count > 0
}


/* 
 * SQLx wrapper for executing transactions.
 * 
 * Example:
 * 		func (s Service) DoSomething() error {
 *    	  return Transaction(func (tx *sqlx.Tx) error {
 * 					  if _, err := tx.Exec(...); err != nil {
 *                return err
 *            }
 *            if _, err := tx.Exec(...); err != nil {
 *                return err
 *            }
 *				})
 *    }
 */
func transaction(conn *sqlx.DB, fn transactionFunc) error {
	tx, err := conn.Beginx()
	if err != nil {
		return err
	}

	defer func() {
		if p := recover(); p != nil {
			switch p := p.(type) {
			case error:
				err = p
			default:
				err = fmt.Errorf("%s", p)
			}
		}
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	return fn(tx)
}


// Returns a cryptographic hash of the given password, its salt, and an
// error, if one occurred.  The password and salt are suitable for storage
// in a database.
func generatePassword(user string, passwd string, args ...string) (string, string, error) {
	var salt []byte

	if len(args) > 0 {
		var derr error
		salt, derr = base64.URLEncoding.DecodeString(args[0])
		if derr != nil {
			return "", "", fmt.Errorf("failed to decode salt")
		}
	} else {
		// Re-use salt size from "entities.go"
		salt = make([]byte, saltSize)
		if _, err := rand.Read(salt); err != nil {
			return "", "", err
		}
	}

	var buffer bytes.Buffer
	buffer.WriteString(user)
	buffer.WriteString(passwd)

	key, err := scrypt.Key(buffer.Bytes(), salt, 16384, 8, 1, 32)
	if err != nil {
		return "", "", err
	}
	ekey  := base64.URLEncoding.EncodeToString(key)
	esalt := base64.URLEncoding.EncodeToString(salt)
	return ekey, esalt, nil
}
