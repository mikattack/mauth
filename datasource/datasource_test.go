package datasource

import (
	"flag"
	"fmt"
	"os"
	"testing"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

const dbfile string = "../testdata/datasource.db"

var (
	conn     *sqlx.DB
	password string = "test"
)

func TestMain(m *testing.M) {
	flag.Parse()
	conn = getDatabaseConnection()
	result := m.Run()
	conn.Close()
	err := os.Remove(dbfile)
	if err != nil {
		panic(fmt.Sprintf("failed to clean up test environment: %s", err.Error()))
	}
	os.Exit(result)
}

func getDatabaseConnection() *sqlx.DB {
	pool, err := sqlx.Open("sqlite3", dbfile)
	if err != nil {
		panic("cannot open test database")
	}
	query := `
		DROP TABLE IF EXISTS mauth_users;
		DROP TABLE IF EXISTS mauth_keys;
	`
	if _, err := pool.Exec(query); err != nil {
		panic(fmt.Sprintf("failed to clean up prior database data: %s", err.Error()))
	}
	if _, err := pool.Exec(DEFAULT_SCHEMA); err != nil {
		panic(fmt.Sprintf("failed to initialize test database: %s", err.Error()))
	}
	return pool
}

func userExists(username string) bool {
	var count int
	query := "SELECT count(name) AS result FROM mauth_users WHERE name=?"
	err := conn.Get(&count, query, username)
	return err == nil && count > 0
}

func insertUser(username string) error {
	query := `INSERT INTO mauth_users (name) VALUES (?)`
	if stmt, err := conn.Prepare(query); err != nil {
		return fmt.Errorf("failed to prepare user data")
	} else if _, err := stmt.Exec(username); err != nil {
		return fmt.Errorf("failed to insert user data")
	}
	return nil
}

func keyCount(username string) int {
	var count int
	query := `SELECT count(key) AS result FROM mauth_keys WHERE user=?`
	err := conn.Get(&count, query, username)
	if err != nil {
		return -1
	}
	return count
}

func reset() error {
	query := `DELETE FROM mauth_users`
	_, err := conn.Exec(query)
	return err
}

func TestUserList(t *testing.T) {
	reset()
	ds := GetDatasource(conn)
	expected := []string{"list_1", "list_2", "list_3"}

	for _, u := range expected {
		if err := insertUser(u); err != nil {
			t.Fatalf("failed to set up list users")
		}
	}

	results, err := ds.List()
	if err != nil {
		t.Errorf("list operation failed: %s", err.Error())
	}
	if len(results) != len(expected) {
		t.Errorf("found %d results, expected %d", len(results), len(expected))
	}
	for index, value := range results {
		if value != expected[index] {
			t.Errorf("expected list entry %d to be '%s', found '%s'", index, expected[index], value)
		}
	}
}

func TestUserExists(t *testing.T) {
	t.Parallel()

	ds := GetDatasource(conn)
	u := "inserttest"
	if err := insertUser(u); err != nil {
		t.Fatalf("%s", err.Error())
	}
	if ds.Exists(u) == false {
		t.Errorf("expected '%s' record to exist", u)
	}
	if ds.Exists("invalid") == true {
		t.Errorf("did not expect 'invalid' record to exist")
	}
}

func TestUserDelete(t *testing.T) {
	t.Parallel()

	ds := GetDatasource(conn)
	u := "deleteable"
	if err := insertUser(u); err != nil {
		t.Fatalf("%s", err.Error())
	}
	if err := ds.Delete(u); err != nil {
		t.Errorf("delete operation failed: %s", err.Error())
	}
	if ds.Exists(u) == true {
		t.Errorf("delete operation failed to remove record")
	}
	if err := ds.Delete("invalid"); err != nil {
		t.Errorf("failed deleting non-exitent record: %s", err.Error())
	}
}

func TestUserCreate(t *testing.T) {
	t.Parallel()

	ds := GetDatasource(conn)
	u := "createable"
	if err := ds.Create(u); err != nil {
		t.Fatalf("add operation failed: %s", err.Error())
	}
	if ds.Exists(u) == false {
		t.Errorf("add operation failed to create record")
	}
	if err := ds.Create(u); err != nil {
		t.Errorf("duplicate add operation not ignored")
	}
}

func TestUserGet(t *testing.T) {
	t.Parallel()

	ds := GetDatasource(conn)
	u := "gettest"
	if err := insertUser(u); err != nil {
		t.Fatalf("failed to set up user fetch test")
	}

	if record, err := ds.Get(u); err != nil {
		t.Errorf("failed to fetch '%s': %s", u, err.Error())
	} else if record.Name != u {
		t.Errorf("expected '%s', found '%s'", u, record.Name)
	}
	if _, err := ds.Get("invalid"); err == nil {
		t.Errorf("failed to fetch invalid record: %s", err.Error())
	}
}

func TestPasswordGenerator(t *testing.T) {
	t.Parallel()

	var salt string
	user := "user"
	pass := "passwd"
	h1, _, e1 := generatePassword(user, pass)
	h2, _, e2 := generatePassword(user, pass)
	if e1 != nil || e2 != nil {
		t.Errorf("password generation failed")
	}
	if h1 == h2 {
		t.Errorf("duplicate generation produced identical results")
	}

	h1, salt, e1 = generatePassword(user, pass)
	h2, _, e2 = generatePassword(user, pass, salt)
	if h1 != h2 {
		t.Errorf("regenerated passwords do not match")
		t.Logf("expected: %s", h1)
		t.Logf("found: %s", h2)
	}
}

func TestChangeUserPassword(t *testing.T) {
	t.Parallel()

	ds := GetDatasource(conn)
	u := "passwordtest"
	if err := insertUser(u); err != nil {
		t.Fatalf("failed to set up password generation test")
	}

	if err := ds.Password(u, password); err != nil {
		t.Errorf("change password failed: %s", err.Error())
	}

	var result struct {
		Passwd string
		Salt   string
	}
	query := `SELECT passwd, salt FROM mauth_users WHERE name=?`
	err := conn.Get(&result, query, u)
	if err != nil {
		t.Errorf("password verification failed: %s", err.Error())
	}
	if len(result.Passwd) == 0 {
		t.Errorf("change verification failed: empty password")
	}
	if len(result.Salt) == 0 {
		t.Errorf("change verification failed: empty salt")
	}
}

func TestAuthenticateUser(t *testing.T) {
	t.Parallel()

	ds := GetDatasource(conn)
	u := "authenticatetest"
	if err := insertUser(u); err != nil {
		t.Fatalf("failed to set up authentication test user")
	}
	if err := ds.Password(u, password); err != nil {
		t.Fatalf("failed to set up authentication test password")
	}

	auth, err := ds.Authenticate("unknown", password)
	if err == nil {
		t.Errorf("expected authentication error for non-existent user")
	}

	auth, err = ds.Authenticate(u, password)
	if err != nil {
		t.Errorf("authentication failure: %s", err.Error())
	}
	if auth == false {
		t.Errorf("expected authentication to succeed")
	}
}

func TestGenerateKey(t *testing.T) {
	var k ApiKey
	u := "keytest"

	ds := GetDatasource(conn)
	if err := insertUser(u); err != nil {
		t.Fatalf("failed to set up key generation test")
	}

	// Check if generating a key for a non-existent user fails
	if _, err := ds.GenerateKey("invalid"); err == nil {
		t.Errorf("GenerateKey() succeeded on non-existent user")
	}

	// Generate a key
	ds.Create(u)
	if key, err := ds.GenerateKey(u); err != nil {
		t.Fatalf("GenerateKey() operation failed: %s", err.Error())
	} else {
		k = key
	}

	// Check whether the key was actually created
	count := keyCount(u)
	if count == -1 {
		t.Fatalf("failed to count keys")
	} else if count != 1 {
		t.Errorf("expected access key for user '%s' not found", u)
	}

	// Check whether a duplicate insertion produces the same key (it should not)
	if key, err := ds.GenerateKey(u); err != nil {
		t.Errorf("failed user insertion during duplicate key test: %s", err.Error())
	} else if key.Key == k.Key {
		t.Errorf("duplicate key insertion produced identical keys")
	}
}

func TestValidateKey(t *testing.T) {
	t.Parallel()

	var k ApiKey
	ds := GetDatasource(conn)
	u := "keyvalidationtest"
	if err := insertUser(u); err != nil {
		t.Fatalf("failed to set up user for key validation test")
	}
	if key, err := ds.GenerateKey(u); err != nil {
		t.Fatalf("failed to generate key for validation test: %s", err.Error())
	} else {
		k = key
	}

	if ds.ValidateKey(k.Key) == false {
		t.Errorf("righteous API key failed validation")
	}
}

func TestRevokeKey(t *testing.T) {
	t.Parallel()

	var k ApiKey
	ds := GetDatasource(conn)
	u := "revokekeytest"
	if err := insertUser(u); err != nil {
		t.Fatalf("failed to set up user for key revocation test")
	}
	if key, err := ds.GenerateKey(u); err != nil {
		t.Fatalf("failed to generate key for revocation test: %s", err.Error())
	} else {
		k = key
	}

	count := keyCount(u)
	if count == -1 {
		t.Fatalf("failed to count keys")
	} else if count != 1 {
		t.Error("expected a single key, found %d", count)
	}

	// Delete a known key
	if err := ds.RevokeKey(k.Key); err != nil {
		t.Errorf("RevokeKey() operation failed: %s", err.Error())
	}

	// Ensure a single key for a user still exists
	count = keyCount(u)
	if count == -1 {
		t.Fatalf("failed to count keys")
	} else if count > 0 {
		t.Errorf("expected 1 key after revocation, found %d", count)
		return
	}
}
