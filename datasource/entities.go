package datasource

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"strconv"
	"time"

	"golang.org/x/crypto/scrypt"
)


const saltSize int = 32


// Represents a user with permission to a protected resource.
type User struct {
	Name	   	string	 	`json:"name"`
	Created 	string		`json:"created"`
	Password	string		`db:"passwd"`
	Salt			string
}


// Represents an API key issued to a given user.
type ApiKey struct {
	User		string
	Key			string
	Created	string
}


func NewApiKey(user string) (ApiKey, error) {
	now := strconv.FormatInt(time.Now().Unix(), 10)
	salt := make([]byte, saltSize)
	_, err := rand.Read(salt)
	if err != nil {
		return ApiKey{}, err
	}

	var buffer bytes.Buffer
	buffer.WriteString(user)
	buffer.WriteString(now)

	key, err := scrypt.Key(buffer.Bytes(), salt, 16384, 8, 1, 32)
	if err != nil {
		return ApiKey{}, err
	}
	ekey := base64.URLEncoding.EncodeToString(key)

	return ApiKey { User:user, Key:ekey, Created:now }, nil
}
