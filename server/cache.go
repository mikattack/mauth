package server

import (
	"sync"
	"time"
)


type cacheManager struct {
	sync.RWMutex
	options			cacheOptions
	sessions		map[string]*session
}

type cacheOptions struct {
	MaxSessions		int
	TTL						int64
}

type session struct {
	Key			string
	Created	int64
}


func newCacheManager(opts cacheOptions) *cacheManager {
	if opts.MaxSessions < 1 {
		opts.MaxSessions = DEFAULT_MAX_SESSIONS
	}
	if opts.TTL < 1 {
		opts.TTL = DEFAULT_SESSION_TTL
	}
	return &cacheManager {
		options:	opts,
		sessions:	map[string]*session{},
	}
}


func (sm *cacheManager) Add(key string) bool {
	sm.Lock()
	if len(sm.sessions) >= sm.options.MaxSessions {
		sm.Unlock()
		return false
	}
	sm.sessions[key] = &session{ Key:key, Created:time.Now().Unix() }
	sm.Unlock()
	return true
}


func (sm *cacheManager) Remove(key string) {
	sm.Lock()
	delete(sm.sessions, key)
	sm.Unlock()
	return
}


func (sm *cacheManager) Exists(key string) bool {
	sm.RLock()
	now := time.Now().Unix()
	entry, exists := sm.sessions[key]
	if exists == false || now - entry.Created < sm.options.TTL {
		exists = false
	}
	sm.RUnlock()
	return exists
}


func (sm *cacheManager) Purge() {
	sm.Lock()
	now := time.Now().Unix()

	// Purge all expired entries (slow, but infrequent and on a fixed size cache)
	for key, s := range sm.sessions {
		if now - s.Created >= sm.options.TTL {
			delete(sm.sessions, key)
		}
	}

	sm.Unlock()
	return
}


func (sm *cacheManager) Renew(key string) {
	sm.Lock()
	if s, exists := sm.sessions[key]; exists == true {
		s.Created = time.Now().Unix()
	}
	sm.Unlock()
	return 
}
