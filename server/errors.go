package server

import "fmt"
import "net/http"


type HttpError struct {
  Code      int
  Prefix    string
  message   string
}

func (e HttpError) Error() string {
  return e.message
}

func NewInternalError(prefix string, message string) HttpError {
  return HttpError { http.StatusInternalServerError, prefix, fmt.Sprintf("%s: %s", prefix, message) }
}

func NewBadInputError(prefix string, message string) HttpError {
  return HttpError { http.StatusBadRequest, prefix, fmt.Sprintf("%s: %s", prefix, message) }
}


type AuthError struct {
	StatusCode	int
	message			string
}

func (e AuthError) Error() string {
  return e.message
}

var (
	invalid_credentials 		= &AuthError{StatusCode:http.StatusBadRequest, message:"invalid login credentials"}
	invalid_cookie					= &AuthError{StatusCode:http.StatusBadRequest, message:"invalid authentication cookie"}
	missing_auth_header			= &AuthError{StatusCode:http.StatusBadRequest, message:"missing authentication header"}
	missing_auth_token			= &AuthError{StatusCode:http.StatusBadRequest, message:"missing authentication token"}
	malformed_auth_header		= &AuthError{StatusCode:http.StatusBadRequest, message:"malformed authentication header"}
	malformed_auth_token		= &AuthError{StatusCode:http.StatusBadRequest, message:"malformed authentication token"}
	internal_error					= &AuthError{StatusCode:http.StatusBadRequest, message:"internal error"}
)
