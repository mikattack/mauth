package server

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/handlers"

	"gitlab.com/mikattack/mauth/datasource"
	"gitlab.com/mikattack/mlog"

	"gopkg.in/tylerb/graceful.v1"
)


const (
	DEFAULT_FLUSH_PERIOD time.Duration = 1 * time.Hour
	DEFAULT_MAX_SESSIONS int = 1000
	DEFAULT_SESSION_TTL int64 = 3660
	DEFAULT_SHUTDOWN_TIMEOUT time.Duration = 30 * time.Second
	max_entity_size int = 1024
	token_name string = "MToken"
)


type Options struct {
	FlushPeriod		time.Duration
	MaxSessions		int
	Port					int
	Realm					string
	TTL						int64
}

type MAuthServer struct {
	cache				*cacheManager
	datasource	*datasource.Datasource
	gserver			*graceful.Server
	mux					*http.ServeMux
	options			Options
	timer				*time.Ticker
}


func New(ds *datasource.Datasource, opts Options) MAuthServer {
	if opts.FlushPeriod == 0 {
		opts.FlushPeriod = 1 * time.Hour
	}

	server := MAuthServer {
		cache:			newCacheManager(cacheOptions{opts.MaxSessions, opts.TTL}),
		datasource:	ds,
		options:		opts,
		timer:			time.NewTicker(opts.FlushPeriod),
	}

	// Ensure cache is purged once an hour
	go func(t *time.Ticker, c *cacheManager) {
		for range t.C {
			c.Purge()
		}
	}(server.timer, server.cache)

	// Create server route handler
	server.mux = http.NewServeMux()
	handler := http.HandlerFunc(server.handler)
	server.mux.Handle("/", handlers.CombinedLoggingHandler(os.Stdout, handler))

	// Create HTTP server (wrapped for graceful interrupt)
	server.gserver = &graceful.Server {
		Timeout: DEFAULT_SHUTDOWN_TIMEOUT,
		Server: &http.Server {
			Addr:         fmt.Sprintf(":%d", opts.Port),
			Handler:      server.mux,
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 10 * time.Second,
		},
	}

	return server
}


func (s *MAuthServer) Start() {
	mlog.INFO.Printf("Starting HTTP authentication server on port %d", s.options.Port)
	s.gserver.ListenAndServe()
}


func (s *MAuthServer) Stop() {
	s.timer.Stop()
	s.gserver.Stop(DEFAULT_SHUTDOWN_TIMEOUT)
}


func (s *MAuthServer) handler(w http.ResponseWriter, r *http.Request) {
	var err error

	switch r.Method {
	case "DELETE":
		err = handleLogoff(s,w,r)
	default:
		err = handleAuthCheck(s,w,r)
	}

	if err != nil {
		switch err.(type) {
		case HttpError:
			// These are expected errors and assumed to be dealt with by request
			// handlers by this point. Relevant logging is determined by handlers.
			// Note that all 4XX errors are coerced into 401 responses.
			code := err.(HttpError).Code
			if code >= 400 && code < 500 {
				// TODO: Classify failures for instrumentation purposes
				code = http.StatusUnauthorized
			}
			w.WriteHeader(code)
			mlog.DEBUG.Printf("%s: %s", err.(HttpError).Prefix, err.Error())
		default:
			mlog.ERROR.Printf("unexpected error: %s", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
		}
	}

	return
}


/* 
 * Evaluates whether a request's "Authorization" header represents an
 * authentic session, which translates to an entry into the local
 * session cache.
 * 
 * A standard 200/401 response (with no content) returned based on success
 * or failure.
 */
func handleAuthCheck(s *MAuthServer, w http.ResponseWriter, r *http.Request) error {
	if hasAuthHeader(r) {
		mlog.DEBUG.Printf("authenticate: header authentication")
		return validateAuthHeader(s, w, r)
	} else if hasAuthCookie(r) {
		mlog.DEBUG.Printf("authenticate: cookie authentication")
		return validateAuthCookie(s, w, r)
	} else {
		// Missing authentication
		mlog.DEBUG.Printf("authenticate: missing token")
		w.Header().Add("WWW-Authenticate", fmt.Sprintf(`Basic realm="%s"`, s.options.Realm))
		w.WriteHeader(http.StatusUnauthorized)
		return nil
	}
}


/* 
 * Deactivates a session.
 */
func handleLogoff(s *MAuthServer, w http.ResponseWriter, r *http.Request) error {
	var token string
	var err error
	if hasAuthHeader(r) {
		token, err = getAuthHeaderToken(r)
	} else if hasAuthCookie(r) {
		token, err = parseAuthCookie(r)
	}
	if err != nil {
		return NewBadInputError("logoff failure", err.Error())
	}
	if token == "" {
		return NewBadInputError("logoff failure", "no token")	
	}
	s.cache.Remove(token)
	w.WriteHeader(http.StatusOK)
	mlog.DEBUG.Println("logoff: expired cache entry")
	return nil
}


func hasAuthHeader(r *http.Request) bool {
	raw := r.Header.Get("Authorization")
	return len(raw) > 0
}


func hasAuthCookie(r *http.Request) bool {
	_, err := r.Cookie(token_name)
	return err == http.ErrNoCookie
}


func validateAuthHeader(s *MAuthServer, w http.ResponseWriter, r *http.Request) error {
	token, err := getAuthHeaderToken(r)
	if err != nil {
		return err
	}
	if user, pass := parseAuthCredentials(token); user != "" && pass != "" {
		// Attempt login and, if successful, set authentication cookie
		return newSession(user, pass, s, w)
	} else {
		// Validate APIKey
		validateToken(token, s, w)
		return nil
	}
}


func validateAuthCookie(s *MAuthServer, w http.ResponseWriter, r *http.Request) error {
	token, err := parseAuthCookie(r)
	if err == nil {
		return err
	}
	validateToken(token, s, w)
	return nil
}


/* 
 * Attempts to establish a new session using the login credentials sent in
 * the request. If the credentials are valid, a session is created and a
 * 200 response returned.  For invalid credentials, a 401 response is returned.
 */
func newSession(user string, passwd string, s *MAuthServer, w http.ResponseWriter) error {
	mlog.DEBUG.Printf("login: attempt for user '%s'", user)

	// Authenticate credentials
	authentic, err := s.datasource.Authenticate(user, passwd)
	if err != nil {
		return NewInternalError("login", err.Error())
	}
	if authentic == false {
		mlog.DEBUG.Println("login: invalid credentials")
		w.WriteHeader(http.StatusUnauthorized)
		return nil
	}

	mlog.DEBUG.Println("login: valid credentials")

	// Generate temporary key and stick it into a cookie for the client
	var token datasource.ApiKey
	token, err = datasource.NewApiKey(user)
	if err != nil {
		return NewInternalError("login", err.Error())
	}
	s.cache.Add(token.Key)
	setAuthCookie(w, token.Key)
	mlog.DEBUG.Println("login: set authentication cookie")
	return nil
}


func getAuthHeaderToken(r *http.Request) (string, error) {
	raw := r.Header.Get("Authorization")
	if len(raw) == 0 {
		mlog.DEBUG.Printf("login: no 'Authorization' header found")
		return "", NewBadInputError("authentication token", "header not found")
	}

	parts := strings.SplitN(raw, " ", 2)
	if len(parts) != 2 || parts[0] != "Basic" {
		mlog.DEBUG.Printf("login: bad 'Authorization' header: %s", raw)
		return "", NewBadInputError("authentication token", "malformed header")
	}

	if len(parts[1]) == 0 {
		mlog.DEBUG.Printf("login: no authentication token supplied")
		return "", NewBadInputError("authentication token", "no token found")
	}

	return parts[1], nil
}


func parseAuthCredentials(token string) (string, string) {
	// Base64 decode the token and see if it resembles a username/password
	decoded, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		mlog.DEBUG.Printf("parseAuthCredentials: base64 decode: %s", err.Error())
		return "", ""
	}
	parts := strings.SplitN(string(decoded), ":", 2)
	if len(parts) != 2 {
		mlog.DEBUG.Printf("parseAuthCredentials: not username/password")
		return "", ""
	} else {
		return parts[0], parts[1]
	}
}


func parseAuthCookie(r *http.Request) (string, error) {
	cookie, err := r.Cookie(token_name)
	if err == http.ErrNoCookie {
		mlog.DEBUG.Printf("no authentication token found in cookie")
		return "", NewBadInputError("authentication cookie", "no authentication token found in cookie")
	}
	return cookie.Value, nil
}


func validateToken(token string, s *MAuthServer, w http.ResponseWriter) {
	if s.cache.Exists(token) {
		mlog.DEBUG.Println("authenticate: cache hit")
		s.cache.Renew(token)
		w.WriteHeader(http.StatusOK)
	} else {
		mlog.DEBUG.Println("authenticate: cache miss")
		if s.datasource.ValidateKey(token) == true {
			mlog.DEBUG.Println("authenticate: cache new session")
			s.cache.Add(token)
			w.WriteHeader(http.StatusOK)
		} else {
			mlog.DEBUG.Println("authenticate: invalid token")
			w.Header().Add("WWW-Authenticate", fmt.Sprintf(`Basic realm="%s"`, s.options.Realm))
			w.WriteHeader(http.StatusUnauthorized)
		}
	}
}


func setAuthCookie(w http.ResponseWriter, token string) {
	rawCookie := fmt.Sprintf("%s=%s", token_name, token)
	cookie := http.Cookie{
		Name:     token_name,
		Value:    token,
		//MaxAge:   259200,
		Secure:   true,
		Raw:      rawCookie,
		Unparsed: []string{rawCookie},
	}
	http.SetCookie(w, &cookie)
}
