package server

import (
	//"bytes"
	"encoding/base64"
	"flag"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/assert"

	"gitlab.com/mikattack/mauth/datasource"
)

/*
 * These are functional tests which exercise the request handlers of a
 * server instance.
 */

const dbfile string = "../testdata/server.db"

var (
	apikey     string
	connection *sqlx.DB
	mauth      MAuthServer
	server     *httptest.Server
	username   string = "test"
	password   string = "example"
)

type requestParams struct {
	Cookie  *http.Cookie
	Headers map[string]string
	Method  string
}

func getDatabaseConnection() *sqlx.DB {
	pool, err := sqlx.Open("sqlite3", dbfile)
	if err != nil {
		panic("cannot open test database")
	}
	sql := `
		DROP TABLE IF EXISTS users;
		DROP TABLE IF EXISTS keys;
	`
	if _, err := pool.Exec(sql); err != nil {
		panic(fmt.Sprintf("failed to clean up prior database data: %s", err.Error()))
	}
	if _, err := pool.Exec(datasource.DEFAULT_SCHEMA); err != nil {
		panic(fmt.Sprintf("failed to initialize test database: %s", err.Error()))
	}
	return pool
}

func request(params requestParams, server *httptest.Server) (*http.Response, error) {
	client := http.Client{}
	req, err := http.NewRequest(params.Method, server.URL, nil)
	if err != nil {
		return nil, err
	}

	for name, value := range params.Headers {
		req.Header.Add(name, value)
	}

	if params.Cookie != nil {
		req.AddCookie(params.Cookie)
	}

	if res, err := client.Do(req); err != nil {
		return nil, err
	} else {
		return res, nil
	}
}

func TestServerOptions(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)

	port := 8888
	realm := "testing"
	opts := Options{Port: port, Realm: realm}
	s := New(datasource.GetDatasource(connection), opts)

	assert.Equal(s.options.FlushPeriod, 1*time.Hour)
	assert.Equal(s.cache.options.MaxSessions, DEFAULT_MAX_SESSIONS)
	assert.Equal(s.cache.options.TTL, DEFAULT_SESSION_TTL)
	assert.Equal(s.options.Port, port)
	assert.Equal(s.options.Realm, realm)

	opts = Options{
		FlushPeriod: time.Duration(0),
		MaxSessions: 0,
		TTL:         0,
	}
	s = New(datasource.GetDatasource(connection), opts)

	assert.Equal(s.options.FlushPeriod, 1*time.Hour)
	assert.Equal(s.cache.options.MaxSessions, DEFAULT_MAX_SESSIONS)
	assert.Equal(s.cache.options.TTL, DEFAULT_SESSION_TTL)
}

func TestCachePurge(t *testing.T) {
	t.Parallel()

	/*
	 * Note that this test takes at least 1.2 seconds to complete, since the
	 * package itself disallows session TTL's less than 1 second.
	 */

	key := "abcd"
	opts := Options{FlushPeriod: 1100 * time.Millisecond, TTL: 1}
	s := New(datasource.GetDatasource(connection), opts)
	s.cache.Add(key)
	time.Sleep(1200 * time.Millisecond)
	assert.Equal(t, false, s.cache.Exists(key))
}

func TestMissingAuthenticationInformation(t *testing.T) {
	// No "Authentication" header and no cookie

	t.Parallel()

	client := http.Client{}
	req, err := http.NewRequest("GET", server.URL, nil)
	if err != nil {
		t.Errorf("cannot create request: %s", err.Error())
	}
	if res, err := client.Do(req); err != nil {
		t.Errorf("failed request: %s", err.Error())
	} else if res.StatusCode != http.StatusUnauthorized {
		t.Errorf("failed response: expected 401, got %d", res.StatusCode)
	}
}

func TestMalformedAuthenticationHeader(t *testing.T) {
	t.Parallel()

	client := http.Client{}
	req, err := http.NewRequest("GET", server.URL, nil)
	req.Header.Add("authorization", "malformed")
	if err != nil {
		t.Errorf("cannot create request: %s", err.Error())
	}
	if res, err := client.Do(req); err != nil {
		t.Errorf("failed request: %s", err.Error())
	} else if res.StatusCode != http.StatusUnauthorized {
		t.Errorf("failed response: expected 401, got %d", res.StatusCode)
	}
}

func TestFailingCookies(t *testing.T) {
	t.Parallel()

	cases := []struct {
		desc  string
		name  string
		value string
	}{
		{"Invalid cookie", "NotCorrectName", "inconsequential"},
		{"Invalid token", "Token", "1234"},
	}
	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			raw := fmt.Sprintf("%s=%s", tc.name, tc.value)
			cookie := http.Cookie{
				Name:     tc.name,
				Value:    tc.value,
				Secure:   true,
				Raw:      raw,
				Unparsed: []string{raw},
			}

			client := http.Client{}
			req, err := http.NewRequest("GET", server.URL, nil)
			req.Header.Add("authorization", "malformed")
			req.AddCookie(&cookie)
			if err != nil {
				t.Errorf("cannot create request: %s", err.Error())
			}
			if res, err := client.Do(req); err != nil {
				t.Errorf("failed request: %s", err.Error())
			} else if res.StatusCode != http.StatusUnauthorized {
				t.Errorf("failed response: expected 401, got %d", res.StatusCode)
			}
		})
	}
}

func TestSuccessfulHeaderAuthentication(t *testing.T) {
	t.Parallel()

	cases := []struct {
		desc     string
		token    string
		expected int
	}{
		{"Cache miss", apikey, http.StatusOK},
		{"Cache hit", apikey, http.StatusOK},
	}
	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			p := requestParams{
				Method: "get",
				Headers: map[string]string{
					"authorization": fmt.Sprintf("Basic %s", tc.token),
				},
			}
			if res, err := request(p, server); err != nil {
				t.Errorf("failed request: %s", err.Error())
			} else if res.StatusCode != tc.expected {
				t.Errorf("failed response: expected %d, got %d", tc.expected, res.StatusCode)
			}
		})
	}
}

func TestSuccessfulCookieAuthentication(t *testing.T) {
	t.Parallel()

	// Create a session with username/password
	headerString := fmt.Sprintf("Basic %s:%s", username, password)
	p := requestParams{
		Method: "get",
		Headers: map[string]string{
			"authorization": base64.URLEncoding.EncodeToString([]byte(headerString)),
		},
	}
	if res, err := request(p, server); err != nil {
		t.Errorf("failed request: %s", err.Error())
	} else if res.StatusCode != http.StatusOK {
		t.Errorf("failed response: expected %d, got %d", http.StatusOK, res.StatusCode)
	}

	/*
		// Authenticate with cookie
		req := httptest.NewRequest("post", server.URL, body)
		w := httptest.NewRecorder()
		if err := handleNewSession(&mauth, w, req); err != nil {
			t.Errorf("login failed: %s", err.Error())
			return
		}
		clone := &http.Request{Header: http.Header{"Cookie": w.HeaderMap["Set-Cookie"]}}
	  cookie, err := clone.Cookie(token_name)
	  if err == http.ErrNoCookie {
			t.Error("login failed: authentication cookie not set")
			return
		}

		/*
		p := requestParams {
			Method:	"get",
			Cookie:	cookie,
		}
		if res, err := request(p, server); err != nil {
			t.Errorf("failed request: %s", err.Error())
		} else if res.StatusCode != http.StatusOK {
			t.Errorf("failed response: expected %d, got %d", http.StatusOK, res.StatusCode)
		}
	*/
}

func TestLogoff(t *testing.T) {

}

func TestValidateAuthHeader(t *testing.T) {

}

func TestParseAuthCredentials(t *testing.T) {
	t.Parallel()

	userpass := fmt.Sprintf("%s:%s", username, password)
	encoded := base64.URLEncoding.EncodeToString([]byte(userpass))

	cases := []struct {
		desc   string
		token  string
		user   string
		passwd string
	}{
		{"invalid data", "", "", ""},
		{"non-user/passwd data", "asdf1234", "", ""},
		{"encoded user/passwd", encoded, username, password},
	}
	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			u, p := parseAuthCredentials(tc.token)
			if u != tc.user || p != tc.passwd {
				t.Errorf("failed response: expected %s/%s, got %s/%s", tc.user, tc.passwd, u, p)
			}
		})
	}
}

func TestMain(m *testing.M) {
	flag.Parse()

	// Set up datasource
	conn := getDatabaseConnection()
	ds := datasource.GetDatasource(conn)

	// Add test user to datasource and generate a key
	var err error
	var key datasource.ApiKey
	err = ds.Create(username)
	err = ds.Password(username, password)
	key, err = ds.GenerateKey(username)
	if err != nil {
		panic(fmt.Sprintf("failed to set up setup test environment: %s", err.Error()))
	}
	apikey = key.Key

	// Set up HTTP server
	mauth = New(ds, Options{})
	connection = conn
	server = httptest.NewServer(mauth.mux)
	defer server.Close()

	// Run tests
	result := m.Run()

	// Clean up datasource
	conn.Close()
	err = os.Remove(dbfile)
	if err != nil {
		panic(fmt.Sprintf("failed to clean up test environment: %s", err.Error()))
	}

	os.Exit(result)
}
